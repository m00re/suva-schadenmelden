package ch.suva.example.schadenmeldung.model.jpa;

import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import lombok.*;
import lombok.experimental.SuperBuilder;

import java.time.LocalDate;
import java.util.UUID;

@Entity
@Table(name = "SCHADENFALL_ENTITY")
@SuperBuilder
@Getter
@NoArgsConstructor
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
public class SchadenfallEntity {

    @Id
    @NonNull
    @EqualsAndHashCode.Include
    @Builder.Default
    private UUID uuid = UUID.randomUUID();

    @NonNull
    private String suvaKundenNr;

    @NonNull
    private LocalDate unfallDatum;

    @NonNull
    private String description;

    @NonNull
    private String vorname;

    @NonNull
    private String nachname;

    @NonNull
    private LocalDate geburtsDatum;

}
