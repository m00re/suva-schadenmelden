package ch.suva.example.schadenmeldung;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SchadenmeldungApplication {

	public static void main(String[] args) {
		SpringApplication.run(SchadenmeldungApplication.class, args);
	}

}
