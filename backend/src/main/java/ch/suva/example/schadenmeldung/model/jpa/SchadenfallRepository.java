package ch.suva.example.schadenmeldung.model.jpa;

import org.springframework.stereotype.Repository;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;

@Repository
public interface SchadenfallRepository extends JpaRepository<SchadenfallEntity, UUID> {
}
