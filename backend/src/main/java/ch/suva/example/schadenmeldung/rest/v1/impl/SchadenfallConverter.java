package ch.suva.example.schadenmeldung.rest.v1.impl;

import ch.suva.example.schadenmeldung.model.jpa.SchadenfallEntity;
import ch.suva.example.schadenmeldung.rest.v1.model.Schadenfall;
import org.springframework.stereotype.Service;

@Service
public class SchadenfallConverter {

    Schadenfall toDto(SchadenfallEntity entity) {
        return new Schadenfall(
                entity.getSuvaKundenNr(),
                entity.getUnfallDatum(),
                entity.getVorname(),
                entity.getNachname(),
                entity.getGeburtsDatum(),
                entity.getDescription()
        ).uuid(entity.getUuid());
    }

    SchadenfallEntity toEntity(Schadenfall dto) {
        return SchadenfallEntity.builder()
                .suvaKundenNr(dto.getSuvaKundenNr())
                .unfallDatum(dto.getUnfallDatum())
                .description(dto.getDescription())
                .vorname(dto.getVorname())
                .nachname(dto.getNachname())
                .geburtsDatum(dto.getGeburtsdatum())
                .build();
    }
}
