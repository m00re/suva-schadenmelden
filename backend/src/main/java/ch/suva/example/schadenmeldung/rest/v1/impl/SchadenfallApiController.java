package ch.suva.example.schadenmeldung.rest.v1.impl;

import ch.suva.example.schadenmeldung.model.jpa.SchadenfallEntity;
import ch.suva.example.schadenmeldung.model.jpa.SchadenfallRepository;
import ch.suva.example.schadenmeldung.rest.v1.api.SchadenfaelleApi;
import ch.suva.example.schadenmeldung.rest.v1.model.Schadenfall;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;

import org.springframework.http.ResponseEntity;

import java.util.List;
import java.util.stream.Collectors;

@Controller
public class SchadenfallApiController implements SchadenfaelleApi {


    private SchadenfallRepository repository;

    private SchadenfallConverter converter;

    @Autowired
    public SchadenfallApiController(SchadenfallRepository repository, SchadenfallConverter converter) {
        this.repository = repository;
        this.converter = converter;
    }

    @Override
    public ResponseEntity<List<Schadenfall>> findSchadenfaelle() {
        return ResponseEntity.ok(
                this.repository.findAll().stream()
                        .map(this.converter::toDto)
                        .collect(Collectors.toList())
        );
    }

    @Override
    public ResponseEntity<Schadenfall> postSchadenfall(Schadenfall schadenfall) {
        final SchadenfallEntity entity = this.converter.toEntity(schadenfall);
        final Schadenfall result = this.converter.toDto(this.repository.save(entity));
        return ResponseEntity
                .status(HttpStatus.CREATED)
                .body(result);
    }
}
