# Very simple Schaden-Melden service

This application comprises a tiny Spring Boot backend and a very basic Angular frontend for a very simple 
Schaden Melden service.

## Requirements

* JDK 17+

## Building & running tests

```bash
./gradlew clean build
```

## Running the service

### Backend application

```bash
./gradlew :backend:bootRun
```

### Frontend application

```bash
./gradlew :frontend:yarnRunServe
```