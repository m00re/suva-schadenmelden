/// <reference types="@angular/localize" />

import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';

import { OpenAPI } from './generated/schaden-melden-v1';
import { AppModule } from './app/app.module';

OpenAPI.CREDENTIALS = 'omit';
OpenAPI.BASE = 'http://localhost:4200';
OpenAPI.HEADERS = {};


platformBrowserDynamic().bootstrapModule(AppModule)
  .catch(err => console.error(err));
