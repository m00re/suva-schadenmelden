import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AddSchadenfallComponent } from './add-schadenfall/add-schadenfall.component';
import { ViewSchadenfallComponent } from './view-schadenfall/view-schadenfall.component';
import { ListSchadenfaelleComponent } from './list-schadenfaelle/list-schadenfaelle.component';

const routes: Routes = [
  { path: '', redirectTo: 'cases', pathMatch: 'full' },
  { path: 'cases', component: ListSchadenfaelleComponent },
  { path: 'cases/add', component: AddSchadenfallComponent },
  { path: 'cases/:id', component: ViewSchadenfallComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
