import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewSchadenfallComponent } from './view-schadenfall.component';

describe('ViewSchadenfallComponent', () => {
  let component: ViewSchadenfallComponent;
  let fixture: ComponentFixture<ViewSchadenfallComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [ViewSchadenfallComponent]
    });
    fixture = TestBed.createComponent(ViewSchadenfallComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
