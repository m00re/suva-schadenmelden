import { Component } from '@angular/core';
import { SchadenfallService } from '../../generated/schaden-melden-v1';
import * as dayjs from 'dayjs';

class SchadenfallModel {

  public suvaKundenNr: string | undefined;
  public unfallDatum: string | undefined;
  public vorname: string | undefined;
  public nachname: string | undefined;
  public geburtsdatum: string | undefined;
  public description: string | undefined;
}

@Component({
  selector: 'app-add-schadenfall',
  templateUrl: './add-schadenfall.component.html',
  styleUrls: ['./add-schadenfall.component.scss']
})
export class AddSchadenfallComponent {

  public model: SchadenfallModel = new SchadenfallModel();

  onSubmit(form: any) {
    const request = {
      uuid: '50f4a671-26e3-464e-a196-f8a9b6c5495e',
      unfallDatum: dayjs(new Date(form.value.unfallDatum.year, form.value.unfallDatum.month-1, form.value.unfallDatum.day)).format('YYYY-MM-DD'),
      suvaKundenNr: form.value.suvaKundenNr,
      vorname: form.value.vorname,
      nachname: form.value.nachname,
      geburtsdatum: dayjs(new Date(form.value.geburtsdatum.year, form.value.geburtsdatum.month-1, form.value.geburtsdatum.day)).format('YYYY-MM-DD'),
      description: form.value.description
    };
    SchadenfallService.postSchadenfall({
      requestBody: request
    }).catch(error => {
      console.log('failed to post new Schadenfall', error);
    });
  }

}
