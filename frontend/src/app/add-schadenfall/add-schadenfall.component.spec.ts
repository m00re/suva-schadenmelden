import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AddSchadenfallComponent } from './add-schadenfall.component';

describe('AddSchadenfallComponent', () => {
  let component: AddSchadenfallComponent;
  let fixture: ComponentFixture<AddSchadenfallComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [AddSchadenfallComponent]
    });
    fixture = TestBed.createComponent(AddSchadenfallComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
