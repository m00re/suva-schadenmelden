import { Component } from '@angular/core';
import { Schadenfall, SchadenfallService } from '../../generated/schaden-melden-v1';

@Component({
  selector: 'app-list-schadenfaelle',
  templateUrl: './list-schadenfaelle.component.html',
  styleUrls: ['./list-schadenfaelle.component.scss']
})
export class ListSchadenfaelleComponent {

  schadenfaelle: Schadenfall[] = [];

  constructor() {
    this.refreshList();
  }

  refreshList() {
    SchadenfallService.findSchadenfaelle()
      .then(
        next => {
          this.schadenfaelle = next;
        },
        error => {
          console.log('Failed to fetch list of Schadenfälle', error);
        });
  }

}
