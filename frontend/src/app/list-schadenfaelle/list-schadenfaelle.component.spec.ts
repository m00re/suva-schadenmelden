import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ListSchadenfaelleComponent } from './list-schadenfaelle.component';

describe('ListSchadenfaelleComponent', () => {
  let component: ListSchadenfaelleComponent;
  let fixture: ComponentFixture<ListSchadenfaelleComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [ListSchadenfaelleComponent]
    });
    fixture = TestBed.createComponent(ListSchadenfaelleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
