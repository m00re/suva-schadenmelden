import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { SchadenfallService } from '../generated/schaden-melden-v1';
import { AddSchadenfallComponent } from './add-schadenfall/add-schadenfall.component';
import { ListSchadenfaelleComponent } from './list-schadenfaelle/list-schadenfaelle.component';
import { ViewSchadenfallComponent } from './view-schadenfall/view-schadenfall.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { RouterModule } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';
import { NgxBootstrapIconsModule, calendar3 } from 'ngx-bootstrap-icons';

@NgModule({
  declarations: [
    AppComponent,
    AddSchadenfallComponent,
    ListSchadenfaelleComponent,
    ViewSchadenfallComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    RouterModule,
    HttpClientModule,
    NgbModule,
    NgxBootstrapIconsModule.pick({ calendar3 }),
    FormsModule
  ],
  providers: [
    SchadenfallService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
